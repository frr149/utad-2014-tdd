//
//  AGTMoney.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 15/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "AGTMoney.h"
#import "AGTBroker.h"

@interface AGTMoney ()
@property (nonatomic) NSNumber *amount;
@end

@implementation AGTMoney

+(id) euroWithAmount:(NSUInteger) amount{
    return [[AGTMoney alloc] initWithAmount:amount currency:@"EUR"];
}

+(id) dollarWithAmount:(NSUInteger) amount{
    return [[AGTMoney alloc] initWithAmount:amount currency:@"USD"];
}

-(id) initWithAmount:(NSUInteger) amount
            currency:(NSString *)currency{
    
    if (self = [super init]) {
        _amount = @(amount);
        _currency = currency;
    }
    return self;
}

-(id) times:(NSUInteger) multiplier{
    
    return [[AGTMoney alloc]
            initWithAmount:[self.amount integerValue] * multiplier
            currency:self.currency] ;
}

-(id)plus:(AGTMoney *) augend{
    
    AGTMoney *sum;
    if ([self.currency isEqualToString:augend.currency]) {
        // Misma divisa y podemos sumar
        NSUInteger total = [self.amount integerValue] + [augend.amount integerValue];
        
        sum = [[AGTMoney alloc] initWithAmount:total
                                      currency:self.currency];
    }else{
        // Divisas distintas: lanzamos excepción
        [NSException raise:@"CurrencyMismatch"
                    format:@"%@ and %@ do not have the same currency!", self, augend];
    }
    return sum;
}

-(AGTMoney *) reduceToCurrency:(NSString *) currency
                    withBroker:(AGTBroker *) broker{
    
    // obtner la tasa
    NSUInteger rate = [broker rateFromCurrency:self.currency
                                    toCurrency:currency];
    
    return [[AGTMoney alloc] initWithAmount:([self.amount integerValue] * rate)
                                                          currency:currency];
    
    
}


#pragma mark - overwrite
-(BOOL)isEqual:(id)object{
    
    return [[self proxyForEquality] isEqual:[object proxyForEquality]];
    
}

-(NSUInteger) hash{
    
    return [self.amount integerValue];
}

-(NSString *)description{
    
    return [NSString stringWithFormat:@"<%@: %@ %@>",
            [self class],
            [self currency],
            [self amount]];
}


-(NSString *)proxyForEquality{
    return [NSString stringWithFormat:@"%@%@",
            self.currency, self.amount];
}





@end

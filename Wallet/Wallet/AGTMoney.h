//
//  AGTMoney.h
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 15/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AGTBroker;
@class AGTMoney;
@protocol AGTMoney <NSObject>

-(id) initWithAmount:(NSUInteger) amount
            currency:(NSString*)currency;
-(id)times:(NSUInteger) multiplier;
-(id)plus:(AGTMoney *) augend;
-(AGTMoney *) reduceToCurrency:(NSString *) currency
                      withBroker:(AGTBroker *) broker;


@end
@interface AGTMoney : NSObject <AGTMoney>
@property (nonatomic, readonly) NSString *currency;
@property (nonatomic, readonly) NSNumber *amount;

+(id) euroWithAmount:(NSUInteger) amount;
+(id) dollarWithAmount:(NSUInteger) amount;




@end




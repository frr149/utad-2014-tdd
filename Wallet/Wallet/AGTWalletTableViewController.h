//
//  AGTMoneyTableViewController.h
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 18/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGTWallet.h"

@interface AGTWalletTableViewController : UITableViewController

@property (nonatomic, strong) AGTWallet *model;
@end

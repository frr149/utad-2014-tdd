//
//  AGTViewController.h
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 18/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTViewController : UIViewController
- (IBAction)displayText:(id)sender forEvent:(UIEvent *)event;
@property (weak, nonatomic) IBOutlet UILabel *displayLabel;

@end

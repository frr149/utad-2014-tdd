//
//  AGTViewController.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 18/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "AGTViewController.h"

@interface AGTViewController ()

@end

@implementation AGTViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)displayText:(id)sender forEvent:(UIEvent *)event {
    
    UIButton * btn = sender;
    self.displayLabel.text = btn.titleLabel.text;
    
   
}
@end

//
//  AGTBroker.h
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 17/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AGTMoney.h"

@interface AGTBroker : NSObject

-(AGTMoney*)reduce:(id<AGTMoney>)money
        toCurrency:(NSString*)currency;

-(void) addRate:(NSUInteger) rate
   fromCurrency:(NSString *)fromCurrency
     toCurrency:(NSString *)toCurrency;

-(NSUInteger) rateFromCurrency:(NSString *) fromCurrency toCurrency:(NSString *) toCurrency;

@end

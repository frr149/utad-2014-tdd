//
//  AGTBroker.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 17/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "AGTBroker.h"

@interface AGTBroker ()
@property (nonatomic, strong) NSMutableDictionary *rates;
@end

@implementation AGTBroker

-(id)init{
    if (self = [super init]) {
        _rates = [@{} mutableCopy];
    }
    return self;
}

-(AGTMoney*)reduce:(id<AGTMoney>)money
        toCurrency:(NSString*)currency{
    
    
    return [money reduceToCurrency:currency
                        withBroker:self];
    
}

-(void) addRate:(NSUInteger) rate
   fromCurrency:(NSString *)fromCurrency
     toCurrency:(NSString *)toCurrency{
    
    [self.rates setObject:@(rate)
                   forKey:[self keyFromCurrency:fromCurrency
                                     toCurrency:toCurrency]];
    [self.rates setObject:@(1)
                   forKey:[self keyFromCurrency:fromCurrency
                                     toCurrency:fromCurrency]];
    
    [self.rates setObject:@(1)
                   forKey:[self keyFromCurrency:toCurrency
                                     toCurrency:toCurrency]];
    
    
}


-(NSUInteger) rateFromCurrency:(NSString *) fromCurrency toCurrency:(NSString *) toCurrency{
    
    NSNumber *rate = [self.rates objectForKey:[self keyFromCurrency:fromCurrency toCurrency:toCurrency]];
    
    return [rate integerValue];
}

#pragma mark - Utils
-(NSString *) keyFromCurrency:(NSString *)fromCurrency
                toCurrency:(NSString *)toCurrency{
    
    return [NSString stringWithFormat:@"%@->%@",
            fromCurrency, toCurrency];
}









@end

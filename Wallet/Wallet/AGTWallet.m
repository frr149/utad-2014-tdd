//
//  AGTWallet.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 17/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "AGTWallet.h"
#import "AGTMoney.h"

@interface AGTWallet()
@property (nonatomic, strong)NSMutableArray *moneys;
@end

@implementation AGTWallet

-(NSUInteger)count{
    return self.moneys.count;
}

-(id)initWithAmount:(NSUInteger)amount currency:(NSString *)currency{
    
    if (self = [super init]) {
        AGTMoney *money = [[AGTMoney alloc] initWithAmount:amount
                                                  currency:currency];
        _moneys = [@[money]mutableCopy];
    }
    return self;
}

-(id) times:(NSUInteger)multiplier{
    
    NSMutableArray *newMoneys = [NSMutableArray
                                 arrayWithCapacity:self.moneys.count];
    for (AGTMoney *money in self.moneys) {
        [newMoneys addObject:[money times:multiplier]];
    }
    self.moneys = newMoneys;
    return self;
}

-(id) plus:(AGTMoney *)augend{
    
    [self.moneys addObject:augend];
    return self;
}

-(AGTMoney *) reduceToCurrency:(NSString *)currency
                    withBroker:(AGTBroker *)broker{
    
    AGTMoney *total = [[AGTMoney alloc] initWithAmount:0
                                              currency:currency];
    for (AGTMoney *money in self.moneys) {
        
        total = [total plus:[money reduceToCurrency:currency
                                         withBroker:broker]];
    }
    
    return total;
}


-(NSString *) description{
    
    return [NSString stringWithFormat:@"<%@:\r%@>", [self class], self.moneys];
}


@end

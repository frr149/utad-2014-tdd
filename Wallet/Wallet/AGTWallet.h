//
//  AGTWallet.h
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 17/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AGTMoney.h"

@interface AGTWallet : NSObject<AGTMoney>
@property (nonatomic, readonly) NSUInteger count;
@end

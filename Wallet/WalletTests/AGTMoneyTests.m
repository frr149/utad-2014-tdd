//
//  AGTMoneyTests.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 16/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AGTMoney.h"
#import "AGTBroker.h"
#import "AGTWallet.h"

@interface AGTMoneyTests : XCTestCase
@property (nonatomic, strong) AGTMoney *oneDollar;
@property (nonatomic, strong) AGTMoney *oneEuro;
@property (nonatomic, strong) AGTBroker *emptyBroker;

@end

@implementation AGTMoneyTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.oneDollar = [AGTMoney dollarWithAmount:1];
    self.oneEuro = [AGTMoney euroWithAmount:1];
    self.emptyBroker = [AGTBroker new];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    self.oneEuro = nil;
    self.oneDollar = nil;
    self.emptyBroker = nil;
}

-(void) testCurrency{
    
    XCTAssertEqualObjects(@"USD", self.oneDollar.currency, @"Currency should be dollars");
    XCTAssertEqualObjects(@"EUR", self.oneEuro.currency, @"Currency should be euros");
    
}


-(void) testMultiplication{
    
    
    AGTMoney *euro = [AGTMoney euroWithAmount:20];
    AGTMoney *total = [AGTMoney euroWithAmount: 40];
    
    euro = [euro times:2];
    
    XCTAssertEqualObjects(euro, total, @"€20 * 2 = €40");
    
    AGTMoney *five = [AGTMoney dollarWithAmount:5];
    AGTMoney *totalDollar = [AGTMoney dollarWithAmount: 10];
    
    XCTAssertEqualObjects([five times:2], totalDollar, @"$5 * 2 = $10");
    
}

-(void) testIsEqual{
    
    AGTMoney *a = [AGTMoney euroWithAmount:1];
    AGTMoney *b = [AGTMoney euroWithAmount:1];
    AGTMoney *c = [AGTMoney euroWithAmount:5];
    
    XCTAssertEqualObjects(a, b, @"Equivalent objects should be equal!");
    XCTAssertNotEqualObjects(a, c, @"Non equivalent objects should not be equal");
    
}

-(void) testHash{
    
    AGTMoney *a = [AGTMoney euroWithAmount:1];
    AGTMoney *b = [AGTMoney euroWithAmount:1];
    
    XCTAssertEqual([a hash], [b hash], @"Equal objects should have the same hash");
    
    
}

-(void) testAmountStorage{
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    
    AGTMoney *one = [AGTMoney euroWithAmount:1];
    NSUInteger value = [[one performSelector:@selector(amount)] integerValue];
    XCTAssertEqual(1, value, @"Value stored and retrieved should be the same!");
#pragma clang diagnostic pop
    
}

-(void)testDifferentCurrencies{
    
    XCTAssertNotEqualObjects(self.oneEuro, self.oneDollar, @"€1 != $1");
}

-(void) testSimpleAddition{
    
    AGTMoney *two = [AGTMoney dollarWithAmount:2];
    
    XCTAssertEqualObjects(two,
                          [self.oneDollar plus: self.oneDollar],
                          @"$1 + $1 = $2");
    
}

-(void) testThatAddingDifferentCurrenciesRaisesException{
    
    XCTAssertThrows([self.oneDollar plus:self.oneEuro],
                    @"Both must have the same currency");
    
}

-(void) testSimpleReduction{
    
    [self.emptyBroker addRate:2 fromCurrency:@"EUR" toCurrency:@"USD"];
    
    AGTMoney *reduced = [self.emptyBroker reduce:self.oneEuro toCurrency:@"EUR"];
    
    XCTAssertEqualObjects(reduced, self.oneEuro, @"Reducing form € to € should leave the object unchanged");
    
}

-(void) testReduction{
    
    [self.emptyBroker addRate:2 fromCurrency:@"EUR" toCurrency:@"USD"];
    
    AGTMoney *fiveEuros = [AGTMoney euroWithAmount:5];
    AGTMoney *tenDollars = [AGTMoney dollarWithAmount:10];
    AGTMoney *reduced = [self.emptyBroker reduce:fiveEuros
                                      toCurrency:@"USD"];
    
    XCTAssertEqualObjects(reduced, tenDollars, @"€5 = $10 2:1");
    
    
}


-(void) testAdditionWithReduction{
    
    [self.emptyBroker addRate:2 fromCurrency:@"EUR" toCurrency:@"USD"];
    
    AGTWallet *wallet = [[AGTWallet alloc] initWithAmount:5 currency:@"EUR"];
    [wallet plus: [AGTMoney dollarWithAmount:10]];
    
    AGTMoney *reduced = [self.emptyBroker reduce:wallet
                                      toCurrency:@"USD"];
    
    XCTAssertEqualObjects(reduced,
                          [AGTMoney dollarWithAmount:20],
                          @"$10 + €5 = $20 2:1");
    
    
    
    
}















@end

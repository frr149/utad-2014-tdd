//
//  AGTViewControllerTests.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 18/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AGTViewController.h"

@interface AGTViewControllerTests : XCTestCase
@property (nonatomic, strong) AGTViewController *vc;
@property (nonatomic, strong) UIButton *btn;
@property (nonatomic, strong) UILabel *label;
@end

@implementation AGTViewControllerTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.vc = [[AGTViewController alloc] initWithNibName:nil bundle:nil];
    self.btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.btn setTitle:@"Hola"
              forState:UIControlStateNormal];
    self.label = [[UILabel alloc] initWithFrame:CGRectZero];
    
    
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    self.vc = nil;
    self.btn = nil;
    self.label = nil;
}

-(void) testThatLabelHasSameTextAsButton{
    
    self.vc.displayLabel = self.label;
    
    [self.vc displayText:self.btn
                forEvent:nil];
    
    XCTAssertEqualObjects(self.btn.titleLabel.text, self.label.text, @"Both strings should be equal");
}

















@end

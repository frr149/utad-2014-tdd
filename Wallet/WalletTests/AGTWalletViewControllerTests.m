//
//  AGTWalletViewControllerTests.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 18/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AGTWallet.h"
#import "AGTWalletTableViewController.h"
@interface AGTWalletViewControllerTests : XCTestCase

@end

@implementation AGTWalletViewControllerTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void) testNumberOfRows{
    
    // Modelo
    AGTWallet *model = [[AGTWallet alloc]initWithAmount:1 currency:@"EUR"];
    [model plus:[AGTMoney dollarWithAmount:1]];
    [model plus:[AGTMoney euroWithAmount:42]];
    
    // Controlador
    AGTWalletTableViewController *wvc = [[AGTWalletTableViewController alloc]init];
    
    wvc.model = model;
    
    NSUInteger numRows = [wvc tableView: nil numberOfRowsInSection:1];
    
    XCTAssertEqual(numRows, model.count + 1, @"Number of rows is always 1 + number of moneys");
    
}

















@end

--
--  AGTAppDelegate.applescript
--  test
--
--  Created by Fernando Rodríguez Romero on 17/07/14.
--  Copyright (c) 2014 Agbo. All rights reserved.
--

script AGTAppDelegate
	property parent : class "NSObject"
	
	-- IBOutlets
	property window : missing value
	
	on applicationWillFinishLaunching_(aNotification)
		-- Insert code here to initialize your application before any files are opened 
	end applicationWillFinishLaunching_
	
	on applicationShouldTerminate_(sender)
		-- Insert code here to do any housekeeping before your application quits 
		return current application's NSTerminateNow
	end applicationShouldTerminate_
	
end script
//
//  main.m
//  test
//
//  Created by Fernando Rodríguez Romero on 17/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <AppleScriptObjC/AppleScriptObjC.h>

int main(int argc, const char * argv[])
{
    [[NSBundle mainBundle] loadAppleScriptObjectiveCScripts];
    return NSApplicationMain(argc, argv);
}
